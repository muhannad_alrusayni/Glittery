Glittery
========
Glittery is a web server that builds static sites. It aims to be
- Simple by Default, Powerfull When Needed
- Generic, suits all kind of people and sites
- Fast, building site should be fast 
- Fully localization support

The project in its early stage and under heavy development.

Wiki
----
Have a look at [manual page](./docs/manual.md).
